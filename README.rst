BBGLab common web framework
===========================

This framework provides to all our websites:

  * Local and remote job execution
  * Users authentication
  * Users profile management
  * Common cherrypy application setup
